# XERO oAuth 2.0 #
This npm library helps you to connect to xero public application and transfer the data.

Kudos to [oauth NPM](https://www.npmjs.com/package/oauth) as this libarary used it's code as a base template.

## Installation ##
```
$ npm install xero-oauth2.0

const xeroOauth = require('xero-oauth2.0')
```
## Constructor ##
```
const oAuthXero = new xeroOauth .xeroOAuth2(
    XERO_APPLICATION_CLIENT_ID,
    XERO_APPLICATION_CLIENT_SECRET,
    CALL_BACK_URL);
```

## Methods ##

1. _getConsentURL(scope)

    *parameters* : scope 

    [link](https://developer.xero.com/documentation/oauth2/scopes) to the full list of scopes for the xero.

    *returns* :  consent url

    ```
    const consentUrl = oAuthXero._getConsentURL(scope);
    ```

2. _getAccessToken
 
    *parameters* : code, callback

    code will return as a part of response upon successful consent

    *callback parameters* : err, accessToken, refreshToken, results

    ```
    oAuthXero._getAccessToken(code, (err, oauthAccessToken, refreshToken, results) => {
        /**
        * your logic sits here
        **/
    })
    ```

3. _refreshToken

    You will be able to refresh the acces token with refresh token if you have offline_access in your token scope. You receive refresh token together with access token.  Access token has short life span (30 min). You can use refresh token to get new access token by doing this you can have uniturupted access to access xero. *Note: You will receive new access token and refresh token everytime your refresh the token*

    *parameters* : refreshToken, callback 

    *callback parameters* : err, oauthAccessToken, refreshToken, results

    ```
    oAuthXero._refreshToken(refreshToken, (err, oauthAccessToken, refreshToken, results) => {
        /**
        * your logic sits here
        **/
    })
    ```

4. _getTenants

    You need tenant id of your organization that you intend to GET/POST data. 

    *parameters* : accessToken, callback

    *callback parameters* : err, data

    data contains array of organization list 

    ```
    oAuthXero._getTenants(accessToken, (err, data) => {
        /**
        * your logic sits here
        **/
    })
    ```

5. _get

    get data from xero

     header options can be passed via options
    
    
    ```
    options = {
        'header' : {
            
        }
    }
    ```

    *parameters* : API type, endPoint, accessToken, tenant id, options, callback

    *callback parameters* : err, data, response

    API Type | Expected parameter to be passed
    ------------ | -------------
    Accounting | accounts
    Payroll - Australia | payroll-au
    Payroll - NZ | payroll-nz
    Payroll - UK | payroll-uk
    Assets | assets
    Projects | projects
    Bank Feeds | bank feeds

    ```
    oAuthXero._get('payroll-au', `/Employees/${employeeId}`, accessToken, tenantId,options, (err, data, response) => {
        /**
        * your logic sits here
        **/
    })
    ```

6. _post

    post data to xero 

    Since Xero havent release its version 2 of api for Payroll Australia, post data content type should be xml

     header options can be passed via options
    
    *content type is required property in the header section

    ```
    options = {
        'header' : {
            'Content-type' : 'application/xml'
        }
    }
    ```

    *parameters* : API type, endPoint, accessToken, xmlPostData, tenant id, options, callback

    *callback parameters* : err, data, response

    ```
    oAuthXero._post('payroll-au', `/Employees/${employeeId}`, xmlPostData, accessToken, tenantId, options, (err, data, response) => {
        /**
        * your logic sits here
        **/
    })
    ```

7. _disconnect

    disconnect from organization 

    *parameters* : tenantId, callback

    *callback parameters* : err, data

    ```
    oAuthXero._disconnect(tenantId, (err, data)=>{
        /**
        * your logic sits here
        **/
    })
    ```

8. _revokeToken

    you can revoke token if you have ofline_acces in the scope/

    ```
    oAuthXero._revokeToken(refreshToken, (err, data)=>{
        /**
        * your logic sits here
        **/
    })
    ```

9. _postAttachment

    Attachment can be uploaded to xero using this method.
    more information follow the [link](https://developer.xero.com/documentation/api/attachments).

    Endpoints that can be uploaded files

     Endpoint | value to be passed as a parameter
    ------------ | -------------     
    Invoices |   '/Invoices/{Guid}/Attachments/{Filename}?{other Options if exist}'
    Receipts |    '/Receipts/{Guid}/Attachments/{Filename}?{other Options if exist}'
    Credit Notes | '/CreditNotes/{Guid}/Attachments/{Filename}?{other Options if exist}'
    Repeating Invoices | '/RepeatingInvoices/{Guid}/Attachments/{Filename}?{other Options if exist}'
    Bank Transactions | '/BankTransactions/{Guid}/Attachments/{Filename}?{other Options if exist}'
    Bank Transfers | '/BankTransfers/{Guid}/Attachments/{Filename}?{other Options if exist}'
    Contacts | '/Contacts/{Guid}/Attachments/{Filename}?{other Options if exist}'
    Accounts | '/Accounts/{Guid}/Attachments/{Filename}?{other Options if exist}'
    Manual Journals | '/ManualJournals/{Guid}/Attachments/{Filename}?{other Options if exist}'
    Purchase Orders | '/PurchaseOrders/{Guid}/Attachments/{Filename}?{other Options if exist}'
    
    header options can be passed via options

    *content type is required property in the header section

    options = {
        'header' : {
            'Content-type' : 'your file content type'
        }
    }

    *parameters* : endPoint, fileContent(Blob type), accessToken,  tenantId, options, callback

    *callback parameters* : err, data, response

    ```
    oAuthXero._postAttachment(endPoint, fileContentType, fileContent, accessToken,  tenantId, options,(err,data, response)=>{
        /**
        * your logic sits here
        **/
    })
    ```





### Notes ##
1. You have to revoke the token if you need to update the scope for the offline access token
2. You can refresh token even before the token expiry.
